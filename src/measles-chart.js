let chart

const measlesData = {
    years: [1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2019],
    cases: [13506, 3124, 1714, 1497, 2587, 2822, 6282, 3655, 3396, 18193, 27786, 9643, 2231, 312, 958, 309, 489, 138, 100, 100, 85, 116, 41, 56, 37, 66, 55, 43, 140, 71, 63, 220, 55, 187, 667, 188, 85, 120, 1250]
}

const searchData = {
    searches: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 32, 47, 67, 44, 76, 88, 94, 336, 437, 232, 294, 313, 304]
}

const intervals = [
    [4, 23],
    [20, 39]
]

let currentInterval = 0

function lookForChart() {
    return new Promise((resolve) => {
        function iLook() {
            const possibleChart = document.getElementById("measles-chart").chart
            if (possibleChart == null) {
                setTimeout(iLook)
            } else {
                chart = possibleChart
                resolve()
            }
        }
        iLook()
    })
}

Reveal.on('ready', event => {
    lookForChart().then(() => updateChartIndex())
})

function updateChartIndex() {
    chart.data.labels = measlesData.years.slice(intervals[currentInterval][0], intervals[currentInterval][1])
    chart.data.datasets[0].data = measlesData.cases.slice(intervals[currentInterval][0], intervals[currentInterval][1])
    chart.update()
}

Reveal.on('fragmentshown', event => {
    if (event.fragment.id.startsWith("measles-chart-t")) {
        currentInterval = parseInt(event.fragment.id.split("measles-chart-t")[1])
        lookForChart().then(updateChartIndex)
    }
    if (event.fragment.id === "measles-chart-searches") {
        chart.data.datasets[1] = {
            data: searchData.searches.slice(intervals[currentInterval][0], intervals[currentInterval][1]),
            label: "Google searches for \"do vaccines cause autism\" (Google Trends)",
            backgroundColor: "rgb(235, 64, 52)"
        }
        chart.update()
    }
})

Reveal.on('fragmenthidden', event => {
    if (event.fragment.id.startsWith("measles-chart-t")) {
        currentInterval = parseInt(event.fragment.id.split("measles-chart-t")[1]) - 1
        lookForChart().then(updateChartIndex)
    }
    if (event.fragment.id === "measles-chart-searches") {
        chart.data.datasets.pop()
    }
    chart.update()
})