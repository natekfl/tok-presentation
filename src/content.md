# Science Vs. Society
Nathan Kutzan

---

<canvas id="measles-chart" data-chart="bar">
<!--
{
 "data": {
  "labels": [],
  "datasets": [
   {
    "data":[],
    "label":"Reported cases of measles in the US (WHO (2019))","backgroundColor":"rgba(20,220,220,.8)"
   }
  ]
 },
 "options": { "responsive": "true" }
}
-->
</canvas>
<p id="measles-chart-t1" class="fragment"></p>
<p id="measles-chart-searches" class="fragment"></p>

Note:
Measles is a highly contagious infectious disease

The death rate in the 1920s was around 30%

In the decade before 1963, nearly all children got measles by the time they were 15 years of age

Among reported cases, an estimated 400 to 500 people died, 48,000 were hospitalized

Scientific advancements and discoveries allowed for the creation of a vaccine.

Following the widespread distribution of vaccines in 1989, infection rates dropped drastically.

By 2000, measles was on track to become the second virus ever to be eradicated by humans.

2011 cases began to rise again

Huge spike in 2019

This is the greatest number of cases reported in the U.S. since 1992

Society became skeptical of vaccines - Antivaxxers

Comparing with google searches we get a pretty clear picture of what caused this increase

---

# Knowledge Question
How does society's perception of trustworthiness towards science affect the impact scientific advancements have on society?

---

# Claim #1
Society tends to value Emotion and Faith as ways of knowing over Reason.

---

### Claim #1 Evidence

Society tends to value Emotion and Faith as ways of knowing over Reason <!-- .element: style="font-size: 0.5em;" -->

- Giordano Bruno was burned at the steak for suggesting that Earth wasn't the center of the universe
- Galileo was sentenced to house arrest for supporting the theory

![Geocentric model vs Heliocentric model](src/solar_system.png)

Notes:
In 1593 Giordano Bruno was charged by the roman inquisition holding opinions contrary to the Catholic faith and speaking against it.

Hanged and burned in 1600.

Roman Inquisition claimed that Galileo was attempting to reinterpret the Bible

February 1616, an Inquisitorial commission declared heliocentrism to be "foolish and absurd in philosophy, and formally heretical since it explicitly contradicts in many places the sense of Holy Scripture". 

Sentenced to house arrest in 1633.

These were scientists who had theories with solid evidence

Rejected by society because they trusted faith more as a way of knowing

---

### Claim #1 Evidence

Society tends to value Emotion and Faith as ways of knowing over Reason <!-- .element: style="font-size: 0.5em;" -->

- Ignaz Semmelweis' ideas about hand washing were rejected because other medical practitioners felt personally attacked.

![Hand Washing](src/hand_washing.png)

Note:
Semmelweis noticed that one hospital of his had very high death rates

He theorized that this death toll could be lowered by surgeons washing their hands between patients

When Semmelweis talked about his findings to other medical practitioners, he was disregarded and accused of calling them dirty

His theories and evidence were rejected on account of being personally offended

Weighs more on emotion

---

### Counterclaim #1
Society's actions against science are caused from a lack of knowledge about science rather than emotion and faith being valued more.

Note:
Claim: Society tends to value Emotion and Faith as ways of knowing over Reason

---

### Counterclaim #1 Evidence

Society's actions against science are caused from a lack of knowledge <!-- .element: style="font-size: 0.5em;" -->

- Gregor Mendel's work on genetic inheritance was rejected initially, but only due to a general lack of knowledge.

![Peas](src/peas.png)

Note:
His work on genetic inheritance wasn’t read by anyone during his life

Scientists struggled to understand him and his theories.

This example proves that there are instances where rejection of ideas stem from more than a value over certain ways of knowing

However, you could argue that this is simply the value of Intuition as a way of knowing.

---

### Conclusion for Claim #1
Society's level of trust for scientific claims is heavily affected by the ways of knowing for which the knowledge is transmitted.

---

# Knowledge Question
How does society's perception of trustworthiness towards science affect the impact scientific advancements have on society?

Note: We've answered what affects a perception of trustworthiness

---

### Claim #2
Scientific discoveries or advancements often require trust and action from the public to have any affect.

---

### Claim #2 Evidence

Scientific discoveries or advancements often require trust and action from the public to have any affect. <!-- .element: style="font-size: 0.5em;" -->

- The hole in the Ozone layer is healing due to widespread trust and action from the public. 

![Ozone Hole](src/ozone.png)

Note:
Since 1985, reductions of up to 70 percent in the ozone column observed

This was due to an overuse of manufactured chemicals, such as refrigerants, solvents, propellants.

In 1987, the United Nations established the Montreal Protocol to regulate the quantities of these gases in the atmosphere. 

The Montreal Protocol was signed by 197 countries, and is the only UN treaty in history to achieve universal ratification.  

It is expected to recover around 2070

This is an example of Science requiring widespread support to solve a problem and make change

---

### Claim #2 Evidence

Scientific discoveries or advancements often require trust and action from the public to have any affect. <!-- .element: style="font-size: 0.5em;" -->

- South Africa’s denial of scientific discoveries linking HIV and AIDS led to more than 330,000 preventable deaths, despite antiretroviral drugs having been developed.

![HIV](src/hiv.png)

Note:
Between 1999 and 2008 Thambo Mbeki (South Africa’s president) denied the link between HIV and AIDS

Despite overwhelming scientific consensus that HIV causes AIDS

he suggested instead that the causes of AIDS are poverty-related and due to poor nourishment and overall ill-health

Instead of pushing for distribution of antiretroviral drugs, the health minister, recommended garlic, beetroot, and lemon juice as treatments for AIDS

Science had the ability to save lives, but a rejection of this advancement caused it to have little effect.

---

### How does society's perception of trustworthiness towards science affect the impact scientific advancements have on society? <!-- .element: style="font-size: 0.8em;" -->

- Society is quick to distrust science because many people place value on ways of knowing that aren't usually inherent to the way scientific information is presented.
- A distrust of science by the public often causes scientific advancements and discoveries to be useless

---

### Relation to current events

- With the COVID-19 virus, it is imperative that society trust science for its advancements to be effective.
- Unfortunately, many people's knowledge about vaccines comes from a place of emotion, from past experiences with vaccines (usually the flu vaccine)
- It will be important for our society to have a better understanding of how knowledge affects our perceptions of science.


